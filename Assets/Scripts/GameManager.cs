﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    public event Action OnCalibration;
    public event Action OnMainMenu;
    public event Action OnTraining;
    public static GameManager instance;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }
    void Start()
    {
        Calibration();
    }

    public void Calibration() 
    {
        OnCalibration?.Invoke();
        Debug.Log("Calibration State");
    }

    public void MainMenu() 
    {
        OnMainMenu?.Invoke();
        Debug.Log("MainMenu State");
    }

    public void Training() 
    {
        OnTraining?.Invoke();
        Debug.Log("Training State");
    }
}

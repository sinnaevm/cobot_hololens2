﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIXRManager : MonoBehaviour
{
    [SerializeField] private GameObject calibrationPanel;
    [SerializeField] private GameObject uIMainMenuCanvas;
    [SerializeField] private GameObject aRContent;

    private void Awake()
    {

    }
    void Start()
    {
        GameManager.instance.OnMainMenu += UIMainMenu;
    }
    
    public void UICalibration() 
    {
        calibrationPanel.SetActive(true);
    }
    public void UIMainMenu() 
    {
        calibrationPanel.SetActive(false);
        uIMainMenuCanvas.transform.position = aRContent.transform.position;
        uIMainMenuCanvas.transform.rotation = aRContent.transform.rotation;
        uIMainMenuCanvas.SetActive(true);

    }
}
